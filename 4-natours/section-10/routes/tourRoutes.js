const express = require('express');
const router = express.Router();
const tourCtl = require('./../controllers/tourController');
const authCtl = require('./../controllers/authController');

router.route('/tour-stats').get(tourCtl.getTourStats);

router.route('/monthly-plan/:year').get(tourCtl.getMonthlyPlan);

router.route('/top-5-cheap').get(tourCtl.aliasTopTours, tourCtl.getAllTours);

router
  .route('/')
  .get(authCtl.protect, tourCtl.getAllTours)
  .post(tourCtl.createTour);

router
  .route('/:id')
  .get(tourCtl.getTour)
  .patch(tourCtl.updateTour)
  .delete(authCtl.protect,
    authCtl.restrictTo('admin', 'lead-guide'),
    tourCtl.deleteTour);

module.exports = router;