const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config({
  path: './config.env'
});

// Set up MongoDB Server
const DSN = 'mongodb+srv://USR:PWD@HOST/NAME?retryWrites=true&w=majority'
  .replace('USR', process.env.DB_USER)
  .replace('PWD', process.env.DB_PASSWD)
  .replace('HOST', process.env.DB_HOST)
  .replace('NAME', process.env.DB_NAME);

// Connect MongoDB
mongoose
  .connect(DSN, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log('DB connection successful!'))
  .catch(error => console.log(error));

const app = require('./app');
//console.log(process.env);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});