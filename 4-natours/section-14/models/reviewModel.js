const mongoose = require('mongoose');
const Tour = require('./../models/tourModel');

const reviewSchema = new mongoose.Schema(
  {
    review: {
      type: String,
      trim: true,
      required: [true, 'Review can not be empty!']
    },
    rating: {
      type: Number,
      min: 1,
      max: 5,
      required: [true, 'You must provide a rating!']
    },
    createdAt: {
      type: Date,
      default: Date.now()
      //select: false // Permanently hide from the API (e.g. passwords!)
    },
    tour: {
      type: mongoose.Schema.ObjectId,
      ref: 'Tour', // Reference parent
      required: [true, 'Review must belong to a tour!']
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: 'User', // Reference parent
      required: [true, 'Review was written by a user!']
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
);
// Limit one review per user per tour.
reviewSchema.index({ tour: 1, user: 1 }, { unique: true });

// Query Middleware
reviewSchema.pre(/^find/, function(next) {
  //   this.populate({
  //     path: 'tour',
  //     select: 'name'
  //   })
  this.populate({
    path: 'user',
    select: 'name photo'
  });
  next();
});

// Set static methods
reviewSchema.statics.calcAverageRatings = async function(tourId) {
  const stats = await this.aggregate([
    {
      $match: { tour: tourId }
    },
    {
      $group: {
        _id: '$tour',
        nRating: { $sum: 1 },
        avgRating: { $avg: '$rating' }
      }
    }
  ]);
  //console.log(stats);

  if (stats.length > 0) {
    await Tour.findByIdAndUpdate(tourId, {
      ratingsQuantity: stats[0].nRating,
      ratingsAverage: stats[0].avgRating
    });
  } else {
    // use schema defaults
    await Tour.findByIdAndUpdate(tourId, {
      ratingsQuantity: 0,
      ratingsAverage: 4.5
    });
  }
};

reviewSchema.post('save', function() {
  // "this" points to current review - need to go up to model!
  this.constructor.calcAverageRatings(this.tour);
});

// For findByIdAndUpdate and findByIdAndDelete, we only have query middleware.
// Let's set up a middleware to access these.
reviewSchema.pre(/^findOneAnd/, async function(next) {
  this.r = await this.findOne(); // Passing to post-middleware below
  //console.log(this.r);
  next();
});
reviewSchema.post(/^findOneAnd/, async function() {
  // await this.findOne();  // Does NOT work here!
  await this.r.constructor.calcAverageRatings(this.r.tour);
});

// Link the schema to a class
const Review = mongoose.model('Review', reviewSchema);
module.exports = Review;
