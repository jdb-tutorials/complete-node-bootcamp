const express = require('express');
const router = express.Router();
const tourCtl = require('./../controllers/tourController');
const authCtl = require('./../controllers/authController');
const reviewRouter = require('./../routes/reviewRoutes');

router.use('/:tourId/reviews', reviewRouter);
router.route('/tour-stats').get(tourCtl.getTourStats);
router.route('/top-5-cheap').get(tourCtl.aliasTopTours, tourCtl.getAllTours);
router
  .route('/monthly-plan/:year')
  .get(
    authCtl.protect,
    authCtl.restrictTo('admin', 'lead-guide', 'guide'),
    tourCtl.getMonthlyPlan
  );

// Calculation routes
router
  .route('/tours-within/:distance/center/:latlng/unit/:unit')
  .get(tourCtl.getToursWithin);
// /tours-within/233/center/40,-45/unit/mi
// as opposed to: /tours-within?distance=233&center=-40,45&unit=mi

router.route('/distances/:latlng/unit/:unit').get(tourCtl.getDistances);

// General routes
router
  .route('/')
  .get(tourCtl.getAllTours)
  .post(
    authCtl.protect,
    authCtl.restrictTo('admin', 'lead-guide'),
    tourCtl.createTour
  );

router
  .route('/:id')
  .get(tourCtl.getTour)
  .patch(
    authCtl.protect,
    authCtl.restrictTo('admin', 'lead-guide'),
    tourCtl.uploadTourImages,
    tourCtl.resizeTourImages,
    tourCtl.updateTour
  )
  .delete(
    authCtl.protect,
    authCtl.restrictTo('admin', 'lead-guide'),
    tourCtl.deleteTour
  );

module.exports = router;
