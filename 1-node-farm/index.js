// Core Modules
const fs = require('fs');
const http = require('http');
const url = require('url');
const slugify = require('slugify'); // parsing for URL friendly strings

// Custom Modules
const replaceTemplate = require(`${__dirname}/modules/replaceTemplate`);

////////////////////////////
// FILES
////////////////////////////

// Blocking, synchronous way
/* const textIn = fs.readFileSync('./txt/input.txt', 'utf-8');
console.log(textIn);
const textOut = `This is what we know about the avocado: ${textIn}.\nCreated on ${Date.now()}`;
fs.writeFileSync('./txt/output.txt', textOut); 
console.log('File written'); */

// Non-blocking, asynchronous way
/*
fs.readFile('./txt/start.txt', 'utf-8', (err, data1) => {
    if (err) return console.log('ERROR! :boom:'); 
    fs.readFile(`./txt/${data1}.txt`, 'utf-8', (err, data2) => {
        console.log(data2);
        fs.readFile('./txt/append.txt', 'utf-8', (err, data3) => {
            console.log(data3);
            fs.writeFile('./txt/final.txt', `${data2}\n${data3}`, 'utf-8', err => {
                console.log('Your file has been written');
            });
        });
    });
});
console.log('Will read file!')
*/

////////////////////////////
// SERVER

// Page Templates
const tempOverview = fs.readFileSync(
  `${__dirname}/templates/template-overview.html`,
  'utf-8'
);
const tempCard = fs.readFileSync(
  `${__dirname}/templates/template-card.html`,
  'utf-8'
);
const tempProduct = fs.readFileSync(
  `${__dirname}/templates/template-product.html`,
  'utf-8'
);

// Dataload
const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, 'utf-8');
const dataObj = JSON.parse(data);
// Rather than use query, convert productName to usable slug
dataObj.map(element => {
  element['slug'] = slugify(element.productName, { lower: true });
});

// Routing
const server = http.createServer((req, res) => {
  const { pathname } = url.parse(req.url, true);

  // Overview page
  if (pathname === '/' || pathname === '/overview') {
    res.writeHead(200, { 'Content-type': 'text/html' });
    const cardsHtml = dataObj
      .map(element => replaceTemplate(tempCard, element))
      .join('');
    const output = tempOverview.replace('{%PRODUCT_CARDS%}', cardsHtml);
    res.end(output);

    // Product page
  } else if (pathname.includes('/product')) {
    res.writeHead(200, { 'Content-type': 'text/html' });
    const slug = pathname.replace('/product/', '');
    const product = dataObj.filter(element => {
      return element.slug === slug;
    })[0];
    const output = replaceTemplate(tempProduct, product);
    res.end(output);

    // API feed
  } else if (pathname === '/api') {
    res.writeHead(200, { 'Content-type': 'application/json' });
    res.end(data);

    // Not found
  } else {
    res.writeHead(404, {
      'Content-type': 'text/html',
      'my-own-header': 'hello-world'
    });
    res.end('<h1>Page not found!</h1>');
  }
  //res.end('Hello from the server!');
});

server.listen(8000, '127.0.0.1', () => {
  console.log('Listening to requests on port 8000');
});
