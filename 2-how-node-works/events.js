const EventEmitter = require("events");
const http = require("http");

class Sales extends EventEmitter {
  constructor() {
    super();
  }
}

const myEmitter = new Sales();

myEmitter.on("newSale", () => {
  console.log("There is a new sale!");
});

myEmitter.on("newSale", () => {
  console.log("Customer Name: Josh");
});

myEmitter.on("newSale", stock => {
  console.log(`There are ${stock} items left in stock.`);
});

myEmitter.emit("newSale", 9);

//////////////////

var clk = 0;
const server = http.createServer();

server.on("request", (req, res) => {
  clk += 1;
  console.log(`Request received ${clk} times`);
  console.log(req.url);
  res.end(`Request received ${clk} times`);
});

server.on("request", (req, res) => {
  res.end("Another request");
});

server.on("close", () => {
  console.log("Server closed");
});

server.listen(8000, "127.0.0.1", () => {
  console.log("Waiting for requests...");
});
