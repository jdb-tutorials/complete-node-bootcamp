const fs = require("fs");
const server = require("http").createServer();

server.on("request", (req, res) => {
  // Solution 1 - Bulk
  // Loads entire file - TOO BIG!
  // Runs out of resources, bogs down, crashes, etc.
  /*
  fs.readFile("test-file.txt", (err, data) => {
    if (err) console.log(err);
    res.end(data)
*/

  // Solution 2 - Stream
  // Better, chunks out the file, but can still bog down loads.
  /*
  const readable = fs.createReadStream("testt-file.txt");
  readable.on("data", chunk => {
    res.write(chunk);
  });
  readable.on("end", () => {
    res.end();
  });
  readable.on("error", err => {
    console.log(err);
    res.statusCode = 500;
    res.end("File not found!");
  });
  */

  // Solution 3 - Piped Stream
  const readable = fs.createReadStream("test-file.txt");
  readable.pipe(res);
});

server.listen(8000, "127.0.0.1", () => {
  console.log("Listening...");
});
